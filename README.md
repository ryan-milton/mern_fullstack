# fullstack_MERN
A simple fullstack MERN application.
___
## Setup and Use
1. To setup this repository locally, clone and ```npm i``` in the root and each repo for good measure.
2. Open two console windows.
3. In one console navigate into the "client" repo and the "backend" repo in the other.
4. In "backend" type ```nodemon server.js```. This will start the server locally and watch for any updates.
5. In "client" type ```npm start```. This will launch the React app locally.

Enjoy.
___
## Issues
If any issues ensue, please contact me and I will get to them ASAP.